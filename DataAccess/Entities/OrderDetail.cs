﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class OrderDetail
    {
        public int? Id { get; set; }
        public string Price { get; set; }
        public string Comment { get; set; }
        public DateTime? Date { get; set; }
        public string NbgRate { get; set; }
        public int LengthId { get; set; } 
        public int OrderId { get; set; }
    }
}
