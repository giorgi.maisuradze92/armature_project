﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;

namespace DataAccess.Entities
{
    public class Company
    {
        public Company()
        {
            CompanyAddressList = new List<CompanyAddress>();
        }
        public int? Id { get; set; }

        [DisplayName("ტიპი")]
        public int Type { get; set; }


        [Write(false)]
        [Computed]
      //  [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public List<CompanyAddress> CompanyAddressList { get; set; }

        [DisplayName("სახელი")]
        public string Title { get; set; }
        [DisplayName("ნომერი")]
        public string IdNumber { get; set; }
    }

    public enum Type
    {
        LegalEntity = 1,
        Individual = 2
    }
}
