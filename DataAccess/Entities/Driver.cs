﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class Driver
    {
        public int? Id { get; set; }

        [DisplayName("სახელი")]
        public string FirstName { get; set; }
        [DisplayName("გვარი")]
        public string LastName { get; set; }
        [DisplayName("პირადი ნომერი")]
        public string PersonalN { get; set; }
        [DisplayName("ტრანსპორტი")]
        public string Transport { get; set; }

    }
}
