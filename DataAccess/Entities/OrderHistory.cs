﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class OrderHistory
    {
        public int? Id { get; set; }
        public int OrderId { get; set; }
        public string HistoryJson { get; set; }
        public DateTime OrderChangeDate { get; set; }
    }
}
