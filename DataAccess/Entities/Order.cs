﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Entities
{
    public class Order
    {
        public Order()
        {
            //Company = new Company();
             DriverList = new List<Driver>();
            //Armatures = new List<Armature>();
            //OrderDetails = new List<OrderDetail>();
        }

        public int? Id { get; set; }
        public int CompanyId { get; set; }  
        public DateTime CreateDate { get; set; }

        public List<Driver> DriverList { get; set; }

        [DisplayFormat(NullDisplayText = "false")]
        public bool? IsPaid { get; set; }

        [DisplayFormat(NullDisplayText = "false")]
        public bool? IsBuyer { get; set; }
        public string Comment { get; set; }
        public int? SendId { get; set; }

        [DisplayFormat(NullDisplayText = "false")]
        public bool? Theoretical { get; set; }
        [DisplayFormat(NullDisplayText = "false")]
        public bool? Khamut { get; set; }
        public string UserName { get; set; }
        public int? CompanyAddress { get; set; }


        //public Company Company { get; set; }
        // public Driver Driver { get; set; }
        //  public List<Armature> Armatures { get; set; }
    }
}