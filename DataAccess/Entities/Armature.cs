﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class Armature
    {
        public Armature()
        {
            //   ArmatureLength = new ArmatureLength();
            //  ArmatureProfile = new ArmatureProfile();
        }
        public int? Id { get; set; }
        public decimal? Amount { get; set; }
        public int ProfileId { get; set; }
        // public decimal? Price { get; set; }

        public int? OrderId { get; set; }
        public int LengthId { get; set; }
        public string Comment { get; set; }

        public bool? Theoretical { get; set; }
        public bool? Khamut { get; set; }
        public bool? Glinula { get; set; }
        //   public ArmatureLength ArmatureLength { get; set; }
        // public ArmatureProfile ArmatureProfile { get; set; }
    }
}
