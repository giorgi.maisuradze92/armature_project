﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Entities;

namespace DataAccess.Services
{
   public interface ICompanyService
    {
        int AddCompany(Company company);
        List<Company> GetAllCompanies();
        Company GetCompany(int Id);
        void DeleteCompany(int Id);
        List<CompanyAddress> GetCompanyAddresses(int companyId);
        int AddCompanyAddress(CompanyAddress companyAddress);

        List<CompanyAddress> GetCompanyAddresses();
        void DeleteCompanyAddress(int id);
    }
}
