﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Entities;
using DataAccess.UnitOfWork;

namespace DataAccess.Services
{
    public class CompanyService : ICompanyService
    {
        private readonly IUnitOfWork _unitOfWork;
        public CompanyService() { }

        public CompanyService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public int AddCompany(Company company)
        {
            return _unitOfWork.CompanyRepository.Save(company);
        }

        public List<Company> GetAllCompanies()
        {
            return _unitOfWork.CompanyRepository.GetAll() as List<Company>;
        }

        public Company GetCompany(int id)
        {
            return _unitOfWork.CompanyRepository.Get(id);
        }

        public int SaveCompany(Company model) {
            return _unitOfWork.CompanyRepository.Save(model);
        }

        public void DeleteCompany(int id)
        {
            _unitOfWork.CompanyRepository.Delete(id);
        }

        public List<CompanyAddress> GetCompanyAddresses(int companyId)
        {
          return  _unitOfWork.CompanyAddressRepository.GetAll(companyId).ToList();
        }

        public int AddCompanyAddress(CompanyAddress companyAddress)
        {
            return _unitOfWork.CompanyAddressRepository.Save(companyAddress);
        }

        public List<CompanyAddress> GetCompanyAddresses()
        {
            return _unitOfWork.CompanyAddressRepository.GetAll().ToList();
        }

        public void DeleteCompanyAddress(int id)
        {
            _unitOfWork.CompanyAddressRepository.Delete(id);
        }
    }
}
