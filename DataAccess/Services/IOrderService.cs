﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Services
{
    public interface IOrderService
    {

        int AddOrder(Order order);

        int SaveOrderHistory(OrderHistory orderHistory);
         
        List<OrderHistory> GetOrderHistoryList(int id);

        int UpdateOrderSendId(int id,string userName);

        void DeleteSendId(int id);

        Entities.Order GetLastOrderByCompanyId(int id);

        void SaveOrderDriver(int orderId, int driverId);

        List<Order> GetAllOrders();
        Order GetOrder(int id);
        void DeleteOrder(int id);
        int AddArmature(Armature armature);
        List<Armature> GetAllArmatures();
        List<Armature> GetAllArmatures(int? id);
        Armature GetArmature(int id);
        void DeleteArmature(int id);
        List<ArmatureLength> GetAllArmatureLength();
        ArmatureLength GetArmatureLength(int id);
        List<ArmatureProfile> GetAllArmatureProfile();
        ArmatureProfile GetArmatureProfile(int id);

        int AddOrderDetail(OrderDetail orderDetail);
        List<OrderDetail> GetAllOrderDetails();
        List<OrderDetail> GetAllOrderDetails(int? id);
        OrderDetail GetOrderDetail(int id);
        void DeleteOrderDetail(int id);

        void DeleteOrderDrivers(int orderId);




    }
}
