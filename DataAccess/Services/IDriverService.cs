﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Services
{
    public interface IDriverService
    {
        int AddDriver(Driver driver);
        List<Driver> GetAllDrivers();
        Driver GetDriver(int id);
        void DeleteDriver(int id);
    }
}
