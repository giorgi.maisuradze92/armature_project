﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Entities;
using DataAccess.UnitOfWork;

namespace DataAccess.Services
{
    public class DriverService : IDriverService
    {
        private readonly IUnitOfWork _unitOfWork;
        public DriverService() { }

        public DriverService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public int AddDriver(Driver driver)
        {
            return _unitOfWork.DriverRepository.Save(driver);
        }

        public void DeleteDriver(int id)
        {
            _unitOfWork.DriverRepository.Delete(id);
        }

        public List<Driver> GetAllDrivers()
        {
            return _unitOfWork.DriverRepository.GetAll() as List<Driver>;
        }

        public Driver GetDriver(int id)
        {
            return _unitOfWork.DriverRepository.Get(id);
        }
    }
}
