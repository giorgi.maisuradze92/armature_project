﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Entities;
using DataAccess.UnitOfWork;

namespace DataAccess.Services
{
    public class OrderService : IOrderService
    {

        private readonly IUnitOfWork _unitOfWork;
        public OrderService() { }

        public OrderService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public int AddArmature(Armature armature)
        {
            return _unitOfWork.ArmatureRepository.Save(armature);
        }

        public int AddOrder(Order order)
        {
            return _unitOfWork.OrderRepository.Save(order);
        }

        public int SaveOrderHistory(OrderHistory orderHistory)
        {
            return _unitOfWork.OrderRepository.SaveOrderHistory(orderHistory);
        }

        public List<OrderHistory> GetOrderHistoryList(int id)
        {
            return _unitOfWork.OrderRepository.GetOrderHistoryList(id);
        }

        public int AddOrderDetail(OrderDetail orderDetail)
        {
            return _unitOfWork.OrderDetailRepository.Save(orderDetail);
        }

        public void DeleteArmature(int id)
        {
            _unitOfWork.ArmatureRepository.Delete(id);
        }

        public void DeleteOrder(int id)
        {
            _unitOfWork.OrderRepository.Delete(id);
        }

        public void DeleteOrderDetail(int id)
        {
            _unitOfWork.OrderDetailRepository.Delete(id);
        }

        public List<ArmatureLength> GetAllArmatureLength()
        {
            return _unitOfWork.ArmatureLengthRepository.GetAll() as List<ArmatureLength>;
        }

        public List<ArmatureProfile> GetAllArmatureProfile()
        {
            return _unitOfWork.ArmatureProfileRepository.GetAll() as List<ArmatureProfile>;
        }

        public List<Armature> GetAllArmatures()
        {
            return _unitOfWork.ArmatureRepository.GetAll() as List<Armature>;
        }

        public List<Armature> GetAllArmatures(int id)
        {
            throw new NotImplementedException();
        }

        public List<Armature> GetAllArmatures(int? id)
        {
            return _unitOfWork.ArmatureRepository.GetAll(id) as List<Armature>;
        }

        public List<OrderDetail> GetAllOrderDetails()
        {
            return _unitOfWork.OrderDetailRepository.GetAll() as List<OrderDetail>;
        }

        public List<OrderDetail> GetAllOrderDetails(int? id)
        {
            return _unitOfWork.OrderDetailRepository.GetAll(id) as List<OrderDetail>;
        }

        public List<Order> GetAllOrders()
        {
            var orderList = _unitOfWork.OrderRepository.GetAll() as List<Order>;

            foreach (var itemOrder in orderList)
            {
                itemOrder.DriverList = _unitOfWork.DriverRepository.GetAllDriverListByOrderId((int)itemOrder.Id);
            }
            return orderList;
        }

        public Armature GetArmature(int id)
        {
            return _unitOfWork.ArmatureRepository.Get(id);
        }

        public ArmatureLength GetArmatureLength(int id)
        {
            return _unitOfWork.ArmatureLengthRepository.Get(id);
        }

         

        public ArmatureProfile GetArmatureProfile(int id)
        {
            return _unitOfWork.ArmatureProfileRepository.Get(id);
        }

        public Order GetLastOrderByCompanyId(int id)
        {
            var order = _unitOfWork.OrderRepository.GetLastOrderByCompanyId(id);
            order.DriverList = _unitOfWork.DriverRepository.GetAllDriverListByOrderId((int)order.Id);
            return order;
        }

        public Order GetOrder(int id)
        {
            var order = _unitOfWork.OrderRepository.Get(id);
            order.DriverList = _unitOfWork.DriverRepository.GetAllDriverListByOrderId(id);
            return order;
        }

        public OrderDetail GetOrderDetail(int id)
        {
            return _unitOfWork.OrderDetailRepository.Get(id);
        }

        public int UpdateOrderSendId(int id, string userName)
        {
            return _unitOfWork.OrderRepository.UpdateOrderSendId(id, userName);
        }

        public void DeleteSendId(int id)
        {
           _unitOfWork.OrderRepository.DeleteSendId(id);
        }

        public void SaveOrderDriver(int orderId, int driverId)
        {
           _unitOfWork.OrderRepository.SaveOrderDriver(driverId,orderId);
        }

        public void DeleteOrderDrivers(int orderId)
        {
           _unitOfWork.OrderRepository.DeleteOrderDrivers(orderId);
        }
    }
}
