﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IOrderRepository : IBaseRepository<Order>
    {
        int UpdateOrderSendId(int id,string userName);

        void DeleteSendId(int id);

        Order GetLastOrderByCompanyId(int id);

        int SaveOrderHistory(OrderHistory orderHistory);

        List<OrderHistory> GetOrderHistoryList(int orderId);

        void SaveOrderDriver(int driverId, int orderId);

        void DeleteOrderDrivers(int orderId);
    }
}
