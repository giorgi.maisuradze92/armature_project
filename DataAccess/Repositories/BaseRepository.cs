﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DataAccess.Infrastructure;

namespace DataAccess.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private IConnectionFactory _connectionFactory;

        public BaseRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public void Delete(int id)
        {
            using (var con = _connectionFactory.GetConnection)
            {
                var query = "spDelete" + typeof(TEntity).Name;
                var param = new DynamicParameters();
                param.Add("@Id", id);
                con.Query(query, param, commandType: CommandType.StoredProcedure);
                con.Close();
            }
        }
        public TEntity Get(int id)
        {
            using (var con = _connectionFactory.GetConnection)
            {
                var query = "spGet" + typeof(TEntity).Name;
                var param = new DynamicParameters();
                param.Add("@Id", id);
                var returnObject = con.Query<TEntity>(query, param,
                       commandType: CommandType.StoredProcedure).FirstOrDefault();
                con.Close();
                return returnObject;
            }
        }

        public IEnumerable<TEntity> GetAll()
        {
            using (var con = _connectionFactory.GetConnection)
            {
                var query = "spGet" + typeof(TEntity).Name + "List";
                var returnObject = con.Query<TEntity>(query, null, commandType: CommandType.StoredProcedure);
                con.Close();
                return returnObject;
            }
        }

        public IEnumerable<TEntity> GetAll(int? id)
        {
            using (var con = _connectionFactory.GetConnection)
            {
                var query = "spGet" + typeof(TEntity).Name + "List";
                var param = new DynamicParameters();
                param.Add("@Id", id);
                var returnObject = con.Query<TEntity>(query, param, commandType: CommandType.StoredProcedure);
                con.Close();
                return returnObject;
            }
        }

        public int Save(TEntity entity)
        {
            using (var con = _connectionFactory.GetConnection)
            {
                var query = "spSave" + typeof(TEntity).Name;
                var returnObject = con.QueryFirst(query, entity, commandType: CommandType.StoredProcedure);

                con.Close();
                return Convert.ToInt32(returnObject.Id);
            }
        }


    }
}
