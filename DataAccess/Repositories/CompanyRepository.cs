﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DataAccess.Entities;
using DataAccess.Infrastructure;

namespace DataAccess.Repositories
{
    public class CompanyRepository : BaseRepository<Company>, ICompanyRepository
    {

        private IConnectionFactory _connectionFactory;
        public CompanyRepository(IConnectionFactory connectionFactory) : base(connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public new int Save(Company entity)
        {
            using (var con = _connectionFactory.GetConnection)
            {
                var query = "spSaveCompany";
                var result = con.QueryFirst(query, new { entity.Id, entity.IdNumber, entity.Title, entity.Type }, commandType: CommandType.StoredProcedure);
                con.Close();
                return Convert.ToInt32(result.Id);
            }
        }
    }
}
