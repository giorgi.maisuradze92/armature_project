﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq; 
using Dapper;
using DataAccess.Infrastructure;

namespace DataAccess.Repositories
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        private readonly IConnectionFactory _connectionFactory;
        public OrderRepository(IConnectionFactory connectionFactory) : base(connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }
        public new int Save(Order entity)
        {
            var param = new
            {
                entity.Id,
                entity.CompanyId,
                entity.CreateDate,
                entity.SendId,
                entity.IsPaid,
                entity.IsBuyer,
                entity.Khamut,
                entity.Theoretical,
                entity.Comment,
                entity.UserName,
                entity.CompanyAddress

            };
            using (var con = _connectionFactory.GetConnection)
            {
                var query = "spSaveOrder";
                var result = con.QueryFirst(query, param, commandType: CommandType.StoredProcedure);
                con.Close();
                return Convert.ToInt32(result.Id);
            }

        }

        public Order GetLastOrderByCompanyId(int id)
        {
            using (var con = _connectionFactory.GetConnection)
            {
                var query = "spGetLastOrderByCompanyId";
                var param = new DynamicParameters();
                param.Add("@CompnayId", id);
                var result = con.QueryFirst<Order>(query, param, commandType: CommandType.StoredProcedure);
                con.Close();
                return result;
            }
        }

        public int SaveOrderHistory(OrderHistory orderHistory)
        {
            using (var con = _connectionFactory.GetConnection)
            {
                var query = "spSaveOrderHistory";
                var result = con.QueryFirst(query, orderHistory, commandType: CommandType.StoredProcedure);
                con.Close();
                return Convert.ToInt32(result.Id);
            }
        }

        public List<OrderHistory> GetOrderHistoryList(int orderId)
        {
            using (var con = _connectionFactory.GetConnection)
            {
                var query = "spGetOrderHistory";
                var param = new DynamicParameters();
                param.Add("@Id", orderId);
                var resut = con.Query<OrderHistory>(query, param, commandType: CommandType.StoredProcedure).ToList();
                con.Close();
                return resut;
            }
        }

        public int UpdateOrderSendId(int id, string userName)
        {
            using (var con = _connectionFactory.GetConnection)
            {
                var query = "UpdateOrderSent";
                var param = new DynamicParameters();
                param.Add("@Id", id);
                param.Add("@UserName", userName);
                var result = con.QueryFirst(query, param,
                    commandType: CommandType.StoredProcedure);
                con.Close();
                return Convert.ToInt32(result.SendId);
            }
        }

        public void DeleteSendId(int id)
        {
            using (var con = _connectionFactory.GetConnection)
            {
                var query = "spDeleteSendId";
                var param = new DynamicParameters();
                param.Add("@Id", id);
                con.Query(query, param, commandType: CommandType.StoredProcedure);
                con.Close();
            }
        }

        public void SaveOrderDriver(int driverId, int orderId)
        {
            using (var con = _connectionFactory.GetConnection)
            {
                var query = "spSaveOrderDriver";
                var param = new DynamicParameters();
                param.Add("@DriverId", driverId);
                param.Add("@OrderId", orderId);
                con.Query(query, param,
                    commandType: CommandType.StoredProcedure);
                con.Close();
            }
        }

        public void DeleteOrderDrivers(int orderId)
        {
            using (var con = _connectionFactory.GetConnection)
            {
                var query = "spDeleteOrderDrivers";
                var param = new DynamicParameters();
                param.Add("@OrderId", orderId);
                con.Query(query, param,
                    commandType: CommandType.StoredProcedure);
                con.Close();
            }
        }
    }
}
