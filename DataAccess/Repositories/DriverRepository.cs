﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DataAccess.Infrastructure;

namespace DataAccess.Repositories
{
    public class DriverRepository : BaseRepository<Driver>, IDriverRepository
    {
        private IConnectionFactory _connectionFactory;
        public DriverRepository(IConnectionFactory connectionFactory) : base(connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public List<Driver> GetAllDriverListByOrderId(int orderId)
        {
            using (var con = _connectionFactory.GetConnection)
            {
                var query = "spGetAllDriverListByOrderId";
                var param = new DynamicParameters();
                param.Add("@Id", orderId);
                var result = con.Query<Driver>(query, param, commandType: CommandType.StoredProcedure).ToList();
                con.Close();
                return result;
            }
        }
    }
}
