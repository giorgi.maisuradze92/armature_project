﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Infrastructure;

namespace DataAccess.Repositories
{
    public class ArmatureLengthRepository : BaseRepository<ArmatureLength>, IArmatureLengthRepository
    {
        public ArmatureLengthRepository(IConnectionFactory connectionFactory) : base(connectionFactory)
        {
        }
    }
}
