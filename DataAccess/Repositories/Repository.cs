﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataAccess.Entities;
using Newtonsoft.Json;

namespace DataAccess.Repositories
{
    public class Repository
    {

        public static User GetUserDetails(User user)
        {
            var path = Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory), "users.txt");
             
            string line;
            using (StreamReader reader = new StreamReader(path))
            {
                line = reader.ReadToEnd();
            }
            var users = JsonConvert.DeserializeObject<List<User>>(line);

            return users.FirstOrDefault(u => u.Email.ToLower() == user.Email.ToLower() &&
                                    u.Password == user.Password);
        }
    }
}
