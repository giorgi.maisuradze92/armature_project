﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Infrastructure;

namespace DataAccess.Repositories
{
    public class ArmatureRepository : BaseRepository<Armature>, IArmatureRepository
    {
        public ArmatureRepository(IConnectionFactory connectionFactory) : base(connectionFactory)
        {
        }
    }
}
