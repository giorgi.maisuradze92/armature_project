﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Entities;
using DataAccess.Infrastructure;

namespace DataAccess.Repositories
{
    public class CompanyAddressRepository : BaseRepository<CompanyAddress>, ICompanyAddressRepository
    {
        public CompanyAddressRepository(IConnectionFactory connectionFactory) : base(connectionFactory)
        {
        }
    }
}
