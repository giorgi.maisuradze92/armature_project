﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Infrastructure;

namespace DataAccess.Repositories
{
    public class ArmatureProfileRepository : BaseRepository<ArmatureProfile>, IArmatureProfileRepository
    {
        public ArmatureProfileRepository(IConnectionFactory connectionFactory) : base(connectionFactory)
        {
        }
    }
}
