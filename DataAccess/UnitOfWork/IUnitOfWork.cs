﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Repositories;

namespace DataAccess.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        ICompanyRepository CompanyRepository { get; }
        IDriverRepository DriverRepository { get; }
        IOrderRepository OrderRepository { get; }
        IArmatureRepository ArmatureRepository { get; }
        IArmatureLengthRepository ArmatureLengthRepository { get; }
        IArmatureProfileRepository ArmatureProfileRepository { get; }

        IOrderDetailRepository OrderDetailRepository { get; }
        ICompanyAddressRepository CompanyAddressRepository { get; }

        void Complete();
    }
}
