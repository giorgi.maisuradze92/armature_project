﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Infrastructure;
using DataAccess.Repositories;

namespace DataAccess.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IDriverRepository _driverRepository;

        private readonly IArmatureLengthRepository _armatureLengthRepository;
        private readonly IArmatureProfileRepository _armatureProfileRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IArmatureRepository _armatureRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;
        private readonly ICompanyAddressRepository _companyAddressRepository;


        public UnitOfWork(ICompanyRepository companyRepository,
                          IDriverRepository driverRepository,
                          IArmatureLengthRepository armatureLengthRepository,
                          IArmatureProfileRepository armatureProfileRepository,
                          IOrderRepository orderRepository,
                          IArmatureRepository armatureRepository,
                          IOrderDetailRepository orderDetailRepository,
                          ICompanyAddressRepository companyAddressRepository)
        {
            _companyRepository = companyRepository;
            _driverRepository = driverRepository;
            _armatureLengthRepository = armatureLengthRepository;
            _armatureProfileRepository = armatureProfileRepository;
            _orderRepository = orderRepository;
            _armatureRepository = armatureRepository;
            _orderDetailRepository = orderDetailRepository;
            _companyAddressRepository = companyAddressRepository;
        }

        void IUnitOfWork.Complete()
        {
            throw new NotImplementedException();
        }

        public ICompanyRepository CompanyRepository
        {
            get
            {
                return _companyRepository;
            }
        }

        public IDriverRepository DriverRepository
        {
            get
            {
                return _driverRepository;
            }
        }

        public ICompanyAddressRepository CompanyAddressRepository => _companyAddressRepository;
         

        public IArmatureLengthRepository ArmatureLengthRepository { get { return _armatureLengthRepository; } }
        public IArmatureProfileRepository ArmatureProfileRepository { get { return _armatureProfileRepository; } }
        public IOrderRepository OrderRepository { get { return _orderRepository; } }
        public IArmatureRepository ArmatureRepository { get { return _armatureRepository; } }

        public IOrderDetailRepository OrderDetailRepository
        {
            get
            {
                return _orderDetailRepository;
            }
        }




        #region IDisposable Support
        private bool disposedValue = false;



        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {

                }
                disposedValue = true;
            }
        }


        void IDisposable.Dispose()
        {
            Dispose(true);

        }
        #endregion
    }
}
