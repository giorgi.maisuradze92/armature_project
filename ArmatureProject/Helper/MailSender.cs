﻿using ArmatureProject.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Runtime.InteropServices;
using System.Web;

namespace ArmatureProject.Helper
{
    public class MailSender
    {

        public void TestWord(OrderPrintModel model)
        {
            GenerateFirstDocument(false, model);
            GenerateSecondDocument(true, model);
        }
        public void SendMail(OrderPrintModel model)
        {

            GenerateFirstDocument(false, model);
            GenerateSecondDocument(false, model);



            SmtpClient smtpClient = new SmtpClient();
            NetworkCredential basicCredential = new NetworkCredential("tkesh11@freeuni.edu.ge", "keshelava3012");
            MailMessage message = new MailMessage();
            MailMessage message2 = new MailMessage();
            MailAddress fromAddress = new MailAddress("tkesh11@freeuni.edu.ge");

            smtpClient.Host = "smtp.gmail.com";
            smtpClient.Port = 587;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = basicCredential;
            smtpClient.Timeout = (60 * 5 * 1000);
            smtpClient.EnableSsl = true;

            message.From = fromAddress;
            message.Subject = model.Order.SendId + " " + model.Company.Title;
            message.IsBodyHtml = false;
            message.Body = model.Order.IsBuyer == true ? "მყიდველი. " : "გამყიდველი. " + model.Order.Comment;
            message.To.Add("tkesh11@freeuni.edu.ge");
            message.To.Add("p.kutateladze@rustavisteel.ge");
            message.To.Add("s.sarsevanidze@rustavisteel.ge");
            message.To.Add("m.tabatadze@rustavisteel.ge");
            message.To.Add("g.macharashvili@rustavisteel.ge");

            var attachmentFilename = @"C:\armature\Info" + model.Order.SendId + ".docx";

            if (attachmentFilename != null)
            {
                Attachment attachment = new Attachment(attachmentFilename, MediaTypeNames.Application.Octet);
                ContentDisposition disposition = attachment.ContentDisposition;
                disposition.CreationDate = File.GetCreationTime(attachmentFilename);
                disposition.ModificationDate = File.GetLastWriteTime(attachmentFilename);
                disposition.ReadDate = File.GetLastAccessTime(attachmentFilename);
                disposition.FileName = Path.GetFileName(attachmentFilename);
                disposition.Size = new FileInfo(attachmentFilename).Length;
                disposition.DispositionType = DispositionTypeNames.Attachment;
                message.Attachments.Add(attachment);
            }

            message2.From = fromAddress;
            message2.Subject = model.Order.SendId + " " + model.Company.Title;
            message2.IsBodyHtml = false;
            message2.Body = model.Order.IsPaid == true ? "გადახდილია. " : "გადასახდელია. " + model.Order.Comment;
            message2.To.Add("tkesh11@freeuni.edu.ge");
            message2.To.Add("p.kutateladze@rustavisteel.ge");
            message2.To.Add("s.sarsevanidze@rustavisteel.ge");
            message2.To.Add("m.tabatadze@rustavisteel.ge");
            message2.To.Add("g.macharashvili@rustavisteel.ge");

            attachmentFilename = @"C:\armature\Info2" + model.Order.SendId + ".docx";

            if (attachmentFilename != null)
            {
                Attachment attachment = new Attachment(attachmentFilename, MediaTypeNames.Application.Octet);
                ContentDisposition disposition = attachment.ContentDisposition;
                disposition.CreationDate = File.GetCreationTime(attachmentFilename);
                disposition.ModificationDate = File.GetLastWriteTime(attachmentFilename);
                disposition.ReadDate = File.GetLastAccessTime(attachmentFilename);
                disposition.FileName = Path.GetFileName(attachmentFilename);
                disposition.Size = new FileInfo(attachmentFilename).Length;
                disposition.DispositionType = DispositionTypeNames.Attachment;
                message2.Attachments.Add(attachment);
                message.Attachments.Add(attachment);
            }

            smtpClient.Send(message);
            smtpClient.Send(message2);
        }



        private void GenerateFirstDocument(bool Visible, OrderPrintModel model)
        {

            #region GenerateWord
            object oMissing = System.Reflection.Missing.Value;
            object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */

            //Start Word and create a new document.
            Microsoft.Office.Interop.Word._Application oWord;
            Microsoft.Office.Interop.Word._Document oDoc;
            oWord = new Microsoft.Office.Interop.Word.Application();
            oWord.Visible = Visible;
            oDoc = oWord.Documents.Add(ref oMissing, ref oMissing,
                ref oMissing, ref oMissing);

            //Insert a paragraph at the beginning of the document.
            Microsoft.Office.Interop.Word.Paragraph head1;
            head1 = oDoc.Content.Paragraphs.Add(ref oMissing);
            head1.Range.Text = "შპს 'რუსთავის ფოლადი'";
            //   oPara1.Range.Font.Bold = 1;
            head1.Format.SpaceAfter = 5;
            head1.Format.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
            head1.Range.InsertParagraphAfter();
            head1.Range.Font.Name = "Sylfaen";
            head1.Range.Font.Size = 9;

            //Insert a paragraph at the end of the document.
            Microsoft.Office.Interop.Word.Paragraph head2;
            object oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            head2 = oDoc.Content.Paragraphs.Add(oRng);
            head2.Range.Text = "გაგარინის ქუჩა N12, რუსთავი 3700, საქართველო";
            head2.Format.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
            head2.Format.SpaceAfter = 6;
            head2.Range.InsertParagraphAfter();

            Microsoft.Office.Interop.Word.Paragraph head3;
            oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            head3 = oDoc.Content.Paragraphs.Add(ref oRng);
            head3.Range.Text = "ტელეფონი: +995 32 2 606669";
            head3.Format.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
            head3.Format.SpaceAfter = 10;
            head3.Range.InsertParagraphAfter();

            //Insert another paragraph.
            Microsoft.Office.Interop.Word.Paragraph head4;
            oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            head4 = oDoc.Content.Paragraphs.Add(ref oRng);
            head4.Range.Text = "არმატურის შეკვეთა N " + model.Order.SendId;
            head4.Range.Font.Italic = 1;
            head4.Range.Font.Underline = Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineSingle;
            head4.Format.SpaceAfter = 5;
            head4.Range.InsertParagraphAfter();

            Microsoft.Office.Interop.Word.Paragraph head5;
            oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            head5 = oDoc.Content.Paragraphs.Add(ref oRng);
            head5.Range.Text = "მყიდველის მონაცემები  თარიღი                                " + model.Order.CreateDate.ToShortDateString();
            head5.Range.Font.Underline = Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineNone;
            head5.Range.Font.Italic = 0;
            head5.Format.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphRight;
            head5.Range.InsertParagraphAfter();


            Microsoft.Office.Interop.Word.Table oTable;
            Microsoft.Office.Interop.Word.Range wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oTable = oDoc.Tables.Add(wrdRng, 4, 4, ref oMissing, ref oMissing);
            oTable.Range.ParagraphFormat.SpaceAfter = 6;
            for (int r = 1; r <= 4; r++)
            {
                for (int c = 1; c <= 4; c++)
                {
                    oTable.Cell(r, c).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
                    oTable.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderBottom].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderTop].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderRight].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderLeft].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                }
            }

            oTable.Cell(1, 1).Range.Text = "მონაცემები";
            oTable.Cell(1, 2).Range.Text = "ფიზიკური პირი";
            oTable.Cell(1, 3).Range.Text = "მონაცემები";
            oTable.Cell(1, 4).Range.Text = "იურიდიული პირი";
            oTable.Cell(2, 1).Range.Text = "სახელი, გვარი";
            oTable.Cell(2, 2).Range.Text = model.Company.Type == 0 ? model.Company.Title : "";
            oTable.Cell(2, 3).Range.Text = "ორგანიზაცია";
            oTable.Cell(2, 4).Range.Text = model.Company.Type != 0 ? model.Company.Title : "";
            oTable.Cell(3, 1).Range.Text = "მისამართი";
            oTable.Cell(3, 2).Range.Text = model.Company.Type == 0 ? model.Company.CompanyAddressList.FirstOrDefault(x => x.Id == model.Order.CompanyAddress)?.Address : "";
            oTable.Cell(3, 3).Range.Text = "იურიდიული მისამართი";
            oTable.Cell(3, 4).Range.Text = model.Company.Type != 0 ? model.Company.CompanyAddressList.FirstOrDefault(x => x.Id == model.Order.CompanyAddress)?.Address : "";
            oTable.Cell(4, 1).Range.Text = "პირადი ნომერი";
            oTable.Cell(4, 2).Range.Text = model.Company.Type == 0 ? model.Company.IdNumber : "";
            oTable.Cell(4, 3).Range.Text = "საიდენთიფიკაციო კოდი";
            oTable.Cell(4, 4).Range.Text = model.Company.Type != 0 ? model.Company.IdNumber : "";
            oTable.Rows[1].Range.Font.Bold = 1;
            oTable.Rows[1].Range.Font.Italic = 1;

            //Add some text after the table.
            Microsoft.Office.Interop.Word.Paragraph oPara4;
            oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oPara4 = oDoc.Content.Paragraphs.Add(ref oRng);
            oPara4.Range.InsertParagraphBefore();
            oPara4.Range.Text = "რაოდენობა";
            oPara4.Format.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
            oPara4.Range.InsertParagraphAfter();


            #region test

            int row = 11;
            int column = 3;

            if (model.Armature.Where(x => x.LengthId == 3).Count() > 0)
            {
                column = 4;
            }

            row = (from p in model.Armature
                      select p.ProfileId).Distinct().Count() + 1;


            Microsoft.Office.Interop.Word.Table oTable4;
            wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oTable4 = oDoc.Tables.Add(wrdRng, row, column, ref oMissing, ref oMissing);
            for (int r = 1; r <= row; r++)
            {
                for (int c = 1; c <= column; c++)
                {
                    oTable4.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderBottom].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable4.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderTop].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable4.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderRight].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable4.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderLeft].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                }
            }

            oTable4.Cell(1, 1).Range.Text = "არმატურის პროფილი";
            oTable4.Cell(1, 2).Range.Text = "12 მეტრი (სტანდარტული, ძირითადი სიგრძე)";
            oTable4.Cell(1, 3).Range.Text = "3 - დან 11 მეტრამდე(არასტანდარტ)";
            if (column == 4)
                oTable4.Cell(1, 4).Range.Text = "1.5 - დან 3 მეტრამდე(არასტანდარტული)";

            //  oTable4.Columns[2].Width = oWord.InchesToPoints((float) 2.5); //Change width of columns 1 & 2
            //    oTable4.Columns[3].Width = oWord.InchesToPoints((float)0.5);
            int counter = 2;
            int[] arr = new int[14];
            string[] names = new string[14];
            names[1] = "6.5 მმ გლინულა";
            names[2] = "8 მმ გლინულა";
            names[10] = "8 მმ";
            names[3] = "10 მმ";
            names[4] = "12 მმ";
            names[5] = "14 მმ";
            names[6] = "22 მმ";
            names[7] = "25 მმ";
            names[8] = "28 მმ";
            names[9] = "32 მმ";
            names[11] = "16 მმ";
            names[12] = "18 მმ";
            names[13] = "20 მმ";

            foreach (var item in model.Armature)
            {
                if (item.LengthId == 1 && item.ProfileId == 1)
                {
                    oTable4.Cell(counter, 1).Range.Text = "6.5 მმ გლინულა";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Glinula + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }
                if (item.LengthId == 1 && item.ProfileId == 2)
                {
                    oTable4.Cell(counter, 1).Range.Text = "8 მმ გლინულა";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Glinula + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 10)
                {
                    oTable4.Cell(counter, 1).Range.Text = "8 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 3)
                {
                    oTable4.Cell(counter, 1).Range.Text = "10 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 4)
                {
                    oTable4.Cell(counter, 1).Range.Text = "12 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 5)
                {
                    oTable4.Cell(counter, 1).Range.Text = "14 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 6)
                {
                    oTable4.Cell(counter, 1).Range.Text = "22 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 7)
                {
                    oTable4.Cell(counter, 1).Range.Text = "25 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 8)
                {
                    oTable4.Cell(counter, 1).Range.Text = "28 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 9)
                {
                    oTable4.Cell(counter, 1).Range.Text = "32 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }
                if (item.LengthId == 1 && item.ProfileId == 11)
                {
                    oTable4.Cell(counter, 1).Range.Text = "16 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 12)
                {
                    oTable4.Cell(counter, 1).Range.Text = "18 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 13)
                {
                    oTable4.Cell(counter, 1).Range.Text = "20 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }
            }
            foreach (var item in model.Armature)
            {
                if (item.LengthId == 2)
                {
                    if (arr[item.ProfileId] == 0)
                    {
                        if (item.ProfileId == 1 || item.ProfileId == 2)
                            oTable4.Cell(counter, 3).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Glinula + item.Comment + ")";
                        else
                            oTable4.Cell(counter, 3).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                        oTable4.Cell(counter, 1).Range.Text = names[item.ProfileId];
                        arr[item.ProfileId] = counter;
                        counter++;
                    }
                    else
                    {
                        if (item.ProfileId == 1 || item.ProfileId == 2)
                            oTable4.Cell(arr[item.ProfileId], 3).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Glinula + item.Comment + ")";
                        else
                            oTable4.Cell(arr[item.ProfileId], 3).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    }
                }
            }


            if (column == 4)
            {
                foreach (var item in model.Armature)
                {
                    if (item.LengthId == 3)
                    {
                        if (arr[item.ProfileId] == 0)
                        {
                            if (item.ProfileId == 1 || item.ProfileId == 2)
                                oTable4.Cell(counter, 4).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Glinula + item.Comment + ")";
                            else
                                oTable4.Cell(counter, 4).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                            oTable4.Cell(counter, 1).Range.Text = names[item.ProfileId];
                            arr[item.ProfileId] = counter;
                            counter++;
                        }
                        else
                        {
                            if (item.ProfileId == 1 || item.ProfileId == 2)
                                oTable4.Cell(arr[item.ProfileId], 4).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Glinula + item.Comment + ")";
                            else
                                oTable4.Cell(arr[item.ProfileId], 4).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                        }
                    }

                }
            }
            #endregion




            //Add some text after the table.
            Microsoft.Office.Interop.Word.Paragraph oPara5;
            oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oPara5 = oDoc.Content.Paragraphs.Add(ref oRng);
            oPara5.Range.Text = "ტონის ფასი (აშშ დოლარი - ექვივალენტი ლარში, გატანის დღეს არსებული ეროვნული ბანკის კურსით";
            oPara4.Format.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft;
            oPara5.Range.Font.Size = 10;
            oPara5.Range.InsertParagraphAfter();

            //ფასის ცხრილი
            Microsoft.Office.Interop.Word.Table oTable3;
            wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oTable3 = oDoc.Tables.Add(wrdRng, 2, 3, ref oMissing, ref oMissing);
            for (int r = 1; r <= 2; r++)
            {
                for (int c = 1; c <= 3; c++)
                {
                    oTable3.Rows[r].Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderTop].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable3.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderBottom].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable3.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderRight].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable3.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderLeft].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                }
            }

            oTable3.Cell(1, 1).Range.Text = "12 მეტრი (სტანდარტული, ძირითადი სიგრძე)";
            oTable3.Cell(1, 2).Range.Text = "3-დან 11 მეტრამდე (არასტანდარტული)";
            oTable3.Cell(1, 3).Range.Text = "1.5-დან 3 მეტრამდე (არასტანდარტული)";
            string first = "";
            string second = "";
            string third = "";


            foreach (var item in model.Detail)
            {
                if (item.LengthId == 1)
                {
                    first += item.Price + "$ " + item.Comment + "\n";
                }
                else if (item.LengthId == 2)
                {
                    second += item.Price + "$ " + item.Comment + "\n";
                }
                else if (item.LengthId == 3)
                {
                    third += item.Price + "$ " + item.Comment + "\n";
                }

            }

            oTable3.Cell(2, 1).Range.Text = first;
            oTable3.Cell(2, 2).Range.Text = second;
            oTable3.Cell(2, 3).Range.Text = third;

            //Add some text after the table.
            Microsoft.Office.Interop.Word.Paragraph oPara7;
            oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oPara7 = oDoc.Content.Paragraphs.Add(ref oRng);
            oPara7.Range.InsertParagraphBefore();
            oPara7.Format.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft;
            oPara7.Range.Text = "გაყიდვების განყოფილების უფროსი:";
            oPara7.Range.ParagraphFormat.SpaceAfter = 6;
            oPara7.Range.InsertParagraphAfter();


            object fileName = @"C:\armature\Info" + model.Order.SendId + ".docx";

            oDoc.SaveAs(ref fileName,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);
            oDoc.Close();
            oWord.Quit();

            Marshal.ReleaseComObject(oWord);
            Marshal.ReleaseComObject(oDoc);
            oWord = null;
            oDoc = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();

            #endregion
            //Close this form.

        }

        public void GenerateSecondDocument(bool Visible, OrderPrintModel model)
        {

            object oMissing = System.Reflection.Missing.Value;
            object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */

            //Start Word and create a new document.
            Microsoft.Office.Interop.Word._Application oWord;
            Microsoft.Office.Interop.Word._Document oDoc;
            oWord = new Microsoft.Office.Interop.Word.Application();
            oWord.Visible = Visible;
            oDoc = oWord.Documents.Add(ref oMissing, ref oMissing,
                ref oMissing, ref oMissing);

            //Insert a paragraph at the beginning of the document.
            Microsoft.Office.Interop.Word.Paragraph head1;
            head1 = oDoc.Content.Paragraphs.Add(ref oMissing);
            head1.Range.Text = "არმატურის დატვირთვის წერილი N                   " + "თარიღი: " + model.Order.CreateDate.ToShortDateString();
            //   oPara1.Range.Font.Bold = 1;
            head1.Format.SpaceAfter = 5;
            head1.Format.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
            head1.Range.InsertParagraphAfter();
            head1.Range.Font.Name = "Sylfaen";
            head1.Range.Font.Size = 8;

            //Insert a paragraph at the end of the document.
            Microsoft.Office.Interop.Word.Paragraph head2;
            object oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            head2 = oDoc.Content.Paragraphs.Add(oRng);
            head2.Range.Text = "მიმღები საამქრო: სორტული საამქრო,";
            head2.Format.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft;
            head2.Format.SpaceAfter = 6;
            head2.Range.InsertParagraphAfter();

            Microsoft.Office.Interop.Word.Paragraph head3;
            oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            head3 = oDoc.Content.Paragraphs.Add(ref oRng);
            head3.Range.Text = "მიმღები პირი: სორტული საამქროს დატვირთვის უბნის უფროსი ჯ. ჯავრაშვილი";
            head3.Format.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft;
            head3.Range.InsertParagraphAfter();

            //Insert another paragraph.
            Microsoft.Office.Interop.Word.Paragraph head4;
            oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            head4 = oDoc.Content.Paragraphs.Add(ref oRng);
            head4.Range.Text = "===============================================================================";
            head4.Range.InsertParagraphAfter();

            Microsoft.Office.Interop.Word.Paragraph head5;
            oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            head5 = oDoc.Content.Paragraphs.Add(ref oRng);
            head5.Range.Text = "მყიდველის მონაცემები";
            head5.Range.Font.Underline = Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineNone;
            head5.Range.Font.Italic = 0;
            head5.Format.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
            head5.Range.InsertParagraphAfter();


            Microsoft.Office.Interop.Word.Table oTable;
            Microsoft.Office.Interop.Word.Range wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oTable = oDoc.Tables.Add(wrdRng, 4, 4, ref oMissing, ref oMissing);
            for (int r = 1; r <= 4; r++)
            {
                for (int c = 1; c <= 4; c++)
                {
                    oTable.Cell(r, c).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
                    oTable.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderBottom].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderTop].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderRight].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderLeft].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                }
            }

            oTable.Cell(1, 1).Range.Text = "მონაცემები";
            oTable.Cell(1, 2).Range.Text = "ფიზიკური პირი";
            oTable.Cell(1, 3).Range.Text = "მონაცემები";
            oTable.Cell(1, 4).Range.Text = "იურიდიული პირი";
            oTable.Cell(2, 1).Range.Text = "სახელი, გვარი";
            oTable.Cell(2, 2).Range.Text = model.Company.Type == 0 ? model.Company.Title : "";
            oTable.Cell(2, 3).Range.Text = "ორგანიზაცია";
            oTable.Cell(2, 4).Range.Text = model.Company.Type != 0 ? model.Company.Title : "";
            oTable.Cell(3, 1).Range.Text = "მისამართი";
            oTable.Cell(3, 2).Range.Text = model.Company.Type == 0 ? model.Company.CompanyAddressList.FirstOrDefault(x => x.Id == model.Order.CompanyAddress)?.Address : "";
            oTable.Cell(3, 3).Range.Text = "იურიდიული მისამართი";
            oTable.Cell(3, 4).Range.Text = model.Company.Type != 0 ? model.Company.CompanyAddressList.FirstOrDefault(x => x.Id == model.Order.CompanyAddress)?.Address : "";
            oTable.Cell(4, 1).Range.Text = "პირადი ნომერი";
            oTable.Cell(4, 2).Range.Text = model.Company.Type == 0 ? model.Company.IdNumber : "";
            oTable.Cell(4, 3).Range.Text = "საიდენთიფიკაციო კოდი";
            oTable.Cell(4, 4).Range.Text = model.Company.Type != 0 ? model.Company.IdNumber : "";
            oTable.Rows[1].Range.Font.Bold = 1;
            oTable.Rows[1].Range.Font.Italic = 1;


            //Add some text after the table.
            Microsoft.Office.Interop.Word.Paragraph oPara4;
            oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oPara4 = oDoc.Content.Paragraphs.Add(ref oRng);
            oPara4.Range.InsertParagraphBefore();
            oPara4.Range.Text = "დასატვირთი პროდუქცია";
            oPara4.Format.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
            oPara4.Range.InsertParagraphAfter();


            #region test

            int row = 11;
            int column = 5;

            if (model.Armature.Where(x => x.LengthId == 3).Count() > 0)
            {
                column = 6;
            }

            row = (from p in model.Armature
                      select p.ProfileId).Distinct().Count() + 1;


            Microsoft.Office.Interop.Word.Table oTable4;
            wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oTable4 = oDoc.Tables.Add(wrdRng, row, column, ref oMissing, ref oMissing);
            for (int r = 1; r <= row; r++)
            {
                for (int c = 1; c <= column; c++)
                {
                    oTable4.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderBottom].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable4.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderTop].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable4.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderRight].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable4.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderLeft].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                }
            }

            oTable4.Cell(1, 1).Range.Text = "არმატურის პროფილი";
            oTable4.Cell(1, 2).Range.Text = "12 მეტრი (სტანდარტული, ძირითადი სიგრძე)";
            oTable4.Cell(1, 3).Range.Text = "3 - დან 11 მეტრამდე(არასტანდარტ)";
            if (column == 6)
            {
                oTable4.Cell(1, 4).Range.Text = "1.5 - დან 3 მეტრამდე(არასტანდარტული)";
                oTable4.Cell(1, 5).Range.Text = "ფაქტიური წონა";
                oTable4.Cell(1, 6).Range.Text = "თეორიული წონა";
            }
            else
            {
                oTable4.Cell(1, 4).Range.Text = "ფაქტიური წონა";
                oTable4.Cell(1, 5).Range.Text = "თეორიული წონა";
            }
            //  oTable4.Columns[2].Width = oWord.InchesToPoints((float) 2.5); //Change width of columns 1 & 2
            //    oTable4.Columns[3].Width = oWord.InchesToPoints((float)0.5);
            int counter = 2;
            int[] arr = new int[14];
            string[] names = new string[14];
            names[1] = "6.5 მმ გლინულა";
            names[2] = "8 მმ გლინულა";
            names[10] = "8 მმ";
            names[3] = "10 მმ";
            names[4] = "12 მმ";
            names[5] = "14 მმ";
            names[6] = "22 მმ";
            names[7] = "25 მმ";
            names[8] = "28 მმ";
            names[9] = "32 მმ";
            names[11] = "16 მმ";
            names[12] = "18 მმ";
            names[13] = "20 მმ";

            foreach (var item in model.Armature)
            {
                if (item.LengthId == 1 && item.ProfileId == 1)
                {
                    oTable4.Cell(counter, 1).Range.Text = "6.5 მმ გლინულა";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Glinula + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }
                if (item.LengthId == 1 && item.ProfileId == 2)
                {
                    oTable4.Cell(counter, 1).Range.Text = "8 მმ გლინულა";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Glinula + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 10)
                {
                    oTable4.Cell(counter, 1).Range.Text = "8 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 3)
                {
                    oTable4.Cell(counter, 1).Range.Text = "10 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 4)
                {
                    oTable4.Cell(counter, 1).Range.Text = "12 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 5)
                {
                    oTable4.Cell(counter, 1).Range.Text = "14 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 6)
                {
                    oTable4.Cell(counter, 1).Range.Text = "22 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 7)
                {
                    oTable4.Cell(counter, 1).Range.Text = "25 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 8)
                {
                    oTable4.Cell(counter, 1).Range.Text = "28 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 9)
                {
                    oTable4.Cell(counter, 1).Range.Text = "32 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 11)
                {
                    oTable4.Cell(counter, 1).Range.Text = "16 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 12)
                {
                    oTable4.Cell(counter, 1).Range.Text = "18 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

                if (item.LengthId == 1 && item.ProfileId == 13)
                {
                    oTable4.Cell(counter, 1).Range.Text = "20 მმ";
                    oTable4.Cell(counter, 2).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                    arr[item.ProfileId] = counter;
                    counter++;
                }

            }
            foreach (var item in model.Armature)
            {
                if (item.LengthId == 2)
                {
                    if (arr[item.ProfileId] == 0)
                    {
                        if (item.ProfileId == 1 || item.ProfileId == 2)
                            oTable4.Cell(counter, 3).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Glinula + item.Comment + ")";
                        else oTable4.Cell(counter, 3).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                        oTable4.Cell(counter, 1).Range.Text = names[item.ProfileId];
                        arr[item.ProfileId] = counter;
                        counter++;
                    }
                    else
                    {
                        if (item.ProfileId == 1 || item.ProfileId == 2)
                            oTable4.Cell(arr[item.ProfileId], 3).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Glinula + item.Comment + ")";
                        else
                            oTable4.Cell(arr[item.ProfileId], 3).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";

                    }
                }
            }


            if (column == 6)
            {
                foreach (var item in model.Armature)
                {
                    if (item.LengthId == 3)
                    {
                        if (arr[item.ProfileId] == 0)
                        {
                            if (item.ProfileId == 1 || item.ProfileId == 2)
                                oTable4.Cell(counter, 4).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Glinula + item.Comment + ")";
                            else
                                oTable4.Cell(counter, 4).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                            oTable4.Cell(counter, 1).Range.Text = names[item.ProfileId];
                            arr[item.ProfileId] = counter;
                            counter++;
                        }
                        else
                        {
                            if (item.ProfileId == 1 || item.ProfileId == 2)
                                oTable4.Cell(arr[item.ProfileId], 4).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Glinula + item.Comment + ")";
                            else
                                oTable4.Cell(arr[item.ProfileId], 4).Range.Text = item.Amount + "(" + model.Khamut + model.Theoretical + item.Comment + ")";
                        }
                    }

                }
            }
            #endregion




            //Add some text after the table.
            Microsoft.Office.Interop.Word.Paragraph oPara5;
            oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oPara5 = oDoc.Content.Paragraphs.Add(ref oRng);
            oPara5.Range.Text = "                                                                              სატრანსპორტო მონაცემები";
            oPara4.Format.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft;
            oPara5.Range.Font.Size = 8;
            oPara5.Range.InsertParagraphAfter();

            //ფასის ცხრილი
            Microsoft.Office.Interop.Word.Table oTable3;
            wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oTable3 = oDoc.Tables.Add(wrdRng, model.DriverList.Count + 2, 4, ref oMissing, ref oMissing);
            for (int r = 1; r <= model.DriverList.Count + 2; r++)
            {
                for (int c = 1; c <= 4; c++)
                {
                    oTable3.Rows[r].Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderTop].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable3.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderBottom].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable3.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderRight].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                    oTable3.Cell(r, c).Range.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderLeft].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                }
            }

            oTable3.Cell(1, 1).Range.Text = "ტრანსპორტი";
            oTable3.Cell(1, 2).Range.Text = "მძღოლი (სახელი, გვარი)";
            oTable3.Cell(1, 3).Range.Text = "პირადობის მოწმობის ნომერი";
            oTable3.Cell(1, 4).Range.Text = "წარმომადგენელი";
            int t = 2;
            for (int i = 0; i < model.DriverList.Count; i++)
            {
                oTable3.Cell(t, 1).Range.Text = model.DriverList[i].Transport;
                oTable3.Cell(t, 2).Range.Text = model.DriverList[i].FirstName + " " + model.DriverList[0].LastName;
                oTable3.Cell(t, 3).Range.Text = model.DriverList[i].PersonalN;
                oTable3.Cell(t, 4).Range.Text = "";
                t++;
            }

            //Add some text after the table.
            Microsoft.Office.Interop.Word.Paragraph oPara6;
            oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oPara6 = oDoc.Content.Paragraphs.Add(ref oRng);
            oPara6.Range.InsertParagraphBefore();
            oPara6.Range.Text = "დატვირთვების ჯგუფის უფროსი:" + Environment.NewLine + "ტვირთების მონიტორინგის ინსპექტორი:" + Environment.NewLine + "გაყიდვების განყოფილების უფროსი:" + Environment.NewLine + "ეკონომიკური უსაფრთხოების დირექტორი:";
            oPara6.Format.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft;
            oPara6.Range.Font.Size = 8;
            oPara6.Range.InsertParagraphAfter();

            //Add some text after the table.
            /*     Microsoft.Office.Interop.Word.Paragraph oPara9;
                 oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                 oPara9 = oDoc.Content.Paragraphs.Add(ref oRng);
                 oPara9.Range.InsertParagraphBefore();
                 oPara9.Range.Text = "ტვირთების მონიტორინგის ინსპექტორი:";
                 oPara9.Range.InsertParagraphAfter();
                 oPara9.Format.SpaceAfter = (float)0.5;

                 //Add some text after the table.
                 Microsoft.Office.Interop.Word.Paragraph oPara7;
                 oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                 oPara7 = oDoc.Content.Paragraphs.Add(ref oRng);
                 oPara7.Range.InsertParagraphBefore();
                 oPara7.Range.Text = "გაყიდვების განყოფილების უფროსი:";
                 oPara7.Range.InsertParagraphAfter();
                 oPara7.Format.SpaceAfter = (float)0.5;

                 Microsoft.Office.Interop.Word.Paragraph oPara8;
                 oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                 oPara8 = oDoc.Content.Paragraphs.Add(ref oRng);
                 oPara8.Range.InsertParagraphBefore();
                 oPara8.Range.Text = ;
                 oPara8.Range.InsertParagraphAfter();*/

            object fileName = @"C:\armature\Info2" + model.Order.SendId + ".docx";

            oDoc.SaveAs(ref fileName,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);
            oDoc.Close();
            oWord.Quit();
            Marshal.ReleaseComObject(oWord);
            Marshal.ReleaseComObject(oDoc);
            oWord = null;
            oDoc = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();

        }


    }
}