﻿using DataAccess.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;
using ArmatureProject.Models;
using System.Xml;
using DataAccess.Entities;
using ArmatureProject.Helper;
using Microsoft.AspNet.Identity;
using Microsoft.Office.Interop.Word;
using Microsoft.Practices.ServiceLocation;
using Newtonsoft.Json;

namespace ArmatureProject.Controllers
{

    [Authorize]
    public class OrderController : Controller
    {

        private IOrderService _orderService;
        private ICompanyService _companyService;
        private IDriverService _driverService;
        public OrderController(IOrderService orderService,
            ICompanyService companyService,
            IDriverService driverService)
        {
            _orderService = orderService;
            _companyService = companyService;
            _driverService = driverService;
        }

        // GET: Order
        public ActionResult Index(int companyId = 0, int driverId = 0, string createDate = "", string endDate = "", int? isSend = 0)
        {
            DateTime startdate;
            DateTime finishdate = new DateTime();

            if (createDate != "" && endDate != "")
            {

                startdate = Convert.ToDateTime(createDate);
                finishdate = Convert.ToDateTime(endDate);
            }
            else
            {
                startdate = DateTime.Now;
            }

            List<Order> orderList;

            if (companyId != 0 && driverId == 0 && createDate == "" && endDate == "" && isSend == 0)
            {
                orderList = _orderService.GetAllOrders().Where(x => x.CompanyId == companyId).ToList();
            }
            else if (companyId == 0 && driverId != 0 && createDate == "" && endDate == "" && isSend == 0)
            {
                orderList = _orderService.GetAllOrders().Where(x => x.DriverList.Any(y => y.Id == driverId)).ToList();
            }
            else if (companyId == 0 && driverId == 0 && createDate == "" && endDate == "" && isSend != 0)
            {
                if (isSend == 2)
                {
                    orderList = _orderService.GetAllOrders().Where(x => x.SendId != null).ToList();
                }
                else
                {
                    orderList = _orderService.GetAllOrders().Where(x => x.SendId == null).ToList();
                }

            }
            else if (companyId == 0 && driverId == 0 && createDate != "" && endDate != "" && isSend == 0)
            {
                orderList = _orderService.GetAllOrders().Where(x => x.CreateDate.Date >= startdate.Date && x.CreateDate.Date < finishdate.Date).ToList();
            }
            else
            {
                orderList = _orderService.GetAllOrders().Where(x => x.CreateDate.Date == startdate.Date).ToList();
            }

            var company = _companyService.GetAllCompanies();
            var driver = _driverService.GetAllDrivers();
            var modelList = new List<OrderListModel>();
            foreach (var item in orderList)
            {
                var model = new OrderListModel();
                model.OrderId = item.Id;

                var armatures = _orderService.GetAllArmatures(item.Id);

                model.SentId = item.SendId == null ? "გასაგზავნი" : item.SendId.ToString();
                model.CompanyName = company.FirstOrDefault(x => x.Id == item.CompanyId)?.Title;
                model.DriverNameList = item.DriverList.Select(x => x.FirstName + " " + x.LastName).ToList();
                //driver.FirstOrDefault(x => x.Id == item.DriverId).FirstName + " " + driver.FirstOrDefault(x => x.Id == item.DriverId).LastName;
                model.CreateDate = item.CreateDate;
                model.ArmatureAmount = armatures.Sum(x => x.Amount);
                modelList.Add(model);
            }
            OrderListModelSearch orderListModelSearch = new OrderListModelSearch();


            orderListModelSearch.CompanyList = company;
            orderListModelSearch.DriverList = driver;

            orderListModelSearch.CompanyList.Add(new Company() { Id = 0, Type = 0, Title = "unknown", IdNumber = "000000000" });
            orderListModelSearch.DriverList.Add(new Driver() { Id = 0, FirstName = "unknown", PersonalN = "00000000", Transport = "unknown" });


            orderListModelSearch.OrderListModel = modelList;


            //  var model1 = modelList.Where(x => x.CreateDate.Date == date.Date).ToList();
            return View(orderListModelSearch);
        }


        public ActionResult Create()
        {
            OrderCreateModel model = new OrderCreateModel();
            var company = _companyService.GetAllCompanies();
            var driver = _driverService.GetAllDrivers();
            var length = _orderService.GetAllArmatureLength();
            var profile = _orderService.GetAllArmatureProfile();
            var companyAddressList = _companyService.GetCompanyAddresses();

            foreach (var item in company)
            {
                item.CompanyAddressList = companyAddressList.Where(x => x.CompanyId == item.Id).ToList();
            }

            model.CompanyList = company;
            model.DriverList = driver;
            model.ArmatureLenghtList = length;
            model.ArmatureProfileList = profile;

            return View(model);
        }


        [HttpPost]
        public ActionResult Create(List<int> driver, int company, bool? isPaid, bool? isBuyer, bool? khamut, bool? theoretical, string comment, int? companyAddress, List<OrderDetailModel> orderDetails, List<Armature> armatures, DateTime orderDate)
        {
            try
            {
                int orderId = _orderService.AddOrder(new Order()
                {
                    CompanyId = company,
                    CreateDate = orderDate,
                    IsPaid = isPaid,
                    IsBuyer = isBuyer,
                    Khamut = khamut,
                    Theoretical = theoretical,
                    Comment = comment,
                    CompanyAddress = companyAddress
                });

                foreach (var item in driver)
                {
                    _orderService.SaveOrderDriver(orderId, item);
                }

                foreach (var item in armatures)
                {
                    var armatureId = _orderService.AddArmature(new Armature()
                    {
                        Comment = item.Comment,
                        LengthId = item.LengthId,
                        OrderId = orderId,
                        Amount = item.Amount,
                        ProfileId = item.ProfileId,
                        Glinula = item.Glinula

                    });
                }

                foreach (var item in orderDetails)
                {
                    if (item.Price != null)
                    {
                        var orderDetailId = _orderService.AddOrderDetail(new OrderDetail()
                        {
                            Comment = item.Comment,
                            OrderId = orderId,
                            LengthId = item.LengthId,
                            Price = item.Price
                        });
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return Exception(ex.Message);
            }
        }


        public ActionResult Exception(string text)
        {
            ViewBag.Ex = text;
            return View();
        }

        [HttpPost]
        public string GetLastOrder(int companyId)
        {
            LastOrderModel model = new LastOrderModel();

            var company = _companyService.GetAllCompanies();
            var driver = _driverService.GetAllDrivers();
            var length = _orderService.GetAllArmatureLength();
            var profile = _orderService.GetAllArmatureProfile();
            var order = _orderService.GetLastOrderByCompanyId(companyId);

            model.Order = order;
            model.Armatures = _orderService.GetAllArmatures(order.Id);
            model.OrderDetails = _orderService.GetAllOrderDetails(order.Id);
            model.CompanyList = company;
            model.DriverList = driver;
            model.ArmatureLenghtList = length;
            model.ArmatureProfileList = profile;

            return JsonConvert.SerializeObject(model);
        }



        public ActionResult OrderHistory(int id)
        {
            var orderHistoryList = _orderService.GetOrderHistoryList(id);
            ViewBag.OrderId = id;
            List<OrderEditModel> model = new List<OrderEditModel>();

            foreach (var item in orderHistoryList)
            {
                var itemModel = JsonConvert.DeserializeObject<OrderEditModel>(item.HistoryJson);
                itemModel.EditDate = item.OrderChangeDate;
                model.Add(itemModel);
            }
            var model1 = new List<WholeOrderModel>();

            foreach (var item in model)
            {
                var armatureList = new List<ArmatureIndexModel>();
                foreach (var armatureItem in item.ArmatureList)
                {
                    armatureList.Add(new ArmatureIndexModel()
                    {
                        Id = armatureItem.Id,
                        Comment = armatureItem.Comment,
                        Profile = item.ArmatureProfileList.FirstOrDefault(x => x.Id == armatureItem.ProfileId).Name,
                        Length = item.ArmatureLengthList.FirstOrDefault(x => x.Id == armatureItem.LengthId).Name,
                        Amount = armatureItem.Amount,
                        Khamut = armatureItem.Khamut,
                        Theoretical = armatureItem.Theoretical,
                        Glinula = armatureItem.Glinula
                    });
                }


                model1.Add(new WholeOrderModel()
                {
                    Order = new Order()
                    {
                        Id = item.OrderId,
                        CompanyId = item.CompanyId,
                        DriverList = item.DriverList.Where(x => item.DriverIdList.Contains(x.Id)).ToList(),
                        CreateDate = item.OrderDate,
                        IsPaid = item.IsPaid,
                        IsBuyer = item.IsBuyer,
                        Comment = item.Comment,
                        SendId = item.SendId,
                        Theoretical = item.Theoretical,
                        Khamut = item.Khamut,
                        CompanyAddress = item.CompanyAddress,

                    },
                    Lenght = item.ArmatureLengthList,
                    ArmatureProfile = item.ArmatureProfileList,
                    OrderDetails = item.OrderDetailList,
                    DriverList = item.DriverList,
                    Armatures = armatureList,
                    CompanyAddress = _companyService.GetCompanyAddresses(item.CompanyId).FirstOrDefault(x => x.Id == item.CompanyAddress)?.Address,
                    Company = item.CompanyList.FirstOrDefault(x => x.Id == item.CompanyId),
                    EditDate = item.EditDate

                });

            }




            return View(model1);
        }


        // GET: Order/Edit/5
        public ActionResult Edit(int id)
        {
            var order = _orderService.GetOrder(id);
            var orderId = id;
            var company = order.CompanyId;
            var driver = order.DriverList;
            var createDate = order.CreateDate;
            var armatures = _orderService.GetAllArmatures(id);
            var orderDetails = _orderService.GetAllOrderDetails(id);
            OrderEditModel model = new OrderEditModel();

            model.OrderId = orderId;
            model.CompanyId = company;
            model.OrderDate = createDate;
            model.DriverIdList = driver.Select(x => x.Id).ToList();
            model.IsBuyer = order.IsBuyer;
            model.IsPaid = order.IsPaid;
            model.Comment = order.Comment;
            model.Theoretical = order.Theoretical;
            model.Khamut = order.Khamut;
            model.CompanyAddress = (int)order.CompanyAddress;
            model.CompanyAddressList = _companyService.GetCompanyAddresses(order.CompanyId);


            model.CompanyList = _companyService.GetAllCompanies();
            model.DriverList = _driverService.GetAllDrivers();
            model.ArmatureLengthList = _orderService.GetAllArmatureLength();
            model.ArmatureProfileList = _orderService.GetAllArmatureProfile();

            model.ArmatureList = armatures;
            model.OrderDetailList = orderDetails;

            var orderHistory = JsonConvert.SerializeObject(model);

            model.OrderHistory = orderHistory;

            return View(model);
        }


        [HttpPost]
        public ActionResult Edit(OrderEditModel model)
        {
            try
            {
                _orderService.SaveOrderHistory(new OrderHistory()
                {
                    OrderId = model.OrderId,
                    HistoryJson = model.OrderHistory,
                    OrderChangeDate = DateTime.Now
                });

                _orderService.DeleteOrderDetail(model.OrderId);
                _orderService.DeleteArmature(model.OrderId);
                _orderService.DeleteOrderDrivers(model.OrderId);

                int orderId = _orderService.AddOrder(new Order()
                {
                    Id = model.OrderId,
                    CompanyId = model.CompanyId,
                    CreateDate = model.OrderDate,
                    Khamut = model.Khamut,
                    Theoretical = model.Theoretical,
                    IsBuyer = model.IsBuyer,
                    IsPaid = model.IsPaid,
                    Comment = model.Comment,
                    CompanyAddress = model.CompanyAddress
                });




                foreach (var item in model.DriverIdList)
                {
                    _orderService.SaveOrderDriver(orderId, (int)item);
                }


                foreach (var item in model.ArmatureList)
                {
                    var armatureId = _orderService.AddArmature(new Armature()
                    {
                        Comment = item.Comment,
                        LengthId = item.LengthId,
                        OrderId = orderId,
                        Amount = item.Amount,
                        Khamut = item.Khamut,
                        Theoretical = item.Theoretical,
                        ProfileId = item.ProfileId,
                        Glinula = item.Glinula
                    });
                }

                foreach (var item in model.OrderDetailList)
                {
                    if (item.Price != null)
                    {
                        var orderDetailId = _orderService.AddOrderDetail(new OrderDetail()
                        {
                            Comment = item.Comment,
                            Date = item.Date,
                            OrderId = orderId,
                            LengthId = item.LengthId,
                            Price = item.Price,
                            NbgRate = item.NbgRate
                        });
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }

        }






        [Authorize(Roles = "Admin")]
        public ActionResult Print(int id)
        {
            try
            {
                _orderService.UpdateOrderSendId(id, User.Identity.Name);
                var orderId = id;
                var order = _orderService.GetOrder(id);
                var company = _companyService.GetCompany(order.CompanyId);
                company.CompanyAddressList = _companyService.GetCompanyAddresses(order.CompanyId);

                var armatures = _orderService.GetAllArmatures(id);
                var orderDetails = _orderService.GetAllOrderDetails(id);

                List<PrintArmature> armaturePrintList = new List<PrintArmature>();

                foreach (var item in armatures)
                {
                    PrintArmature pr = new PrintArmature();
                    pr.Id = item.Id;
                    pr.Comment = item.Comment;
                    pr.Amount = item.Amount;
                    pr.LengthId = item.LengthId;
                    pr.ProfileId = item.ProfileId;
                    pr.OrderId = item.OrderId;
                    pr.Glinula = item.Glinula == true ? "დახვეული " : "გასწორებული";

                    armaturePrintList.Add(pr);
                }
                OrderPrintModel model = new OrderPrintModel();

                model.Order = order;
                model.Company = company;
                model.DriverList = order.DriverList;
                model.Armature = armaturePrintList;
                model.Detail = orderDetails;
                model.Theoretical = order.Theoretical == true ? "თეორიულით, " : "რეალური";
                model.Khamut = order.Khamut == true ? "ხამუტებით, " : "";


                MailSender m = new MailSender();
                m.SendMail(model);
            }
            catch (Exception ex)
            {
                string name = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                _orderService.DeleteSendId(id);
                ViewBag.Ex = ex;
                ViewBag.User = name;
                return View("Exception");
                // return RedirectToAction("index");
            }



            return RedirectToAction("index");
        }



        // GET: Driver/Details/5
        public ActionResult Details(int id)
        {

            var order = _orderService.GetOrder(id);
            var armatures = _orderService.GetAllArmatures(order.Id);
            var orderDetails = _orderService.GetAllOrderDetails(order.Id);
            var company = _companyService.GetAllCompanies().FirstOrDefault(x => x.Id == order.CompanyId);
            company.CompanyAddressList = _companyService.GetCompanyAddresses(order.CompanyId);
            var length = _orderService.GetAllArmatureLength();
            var profile = _orderService.GetAllArmatureProfile();

            List<ArmatureIndexModel> armatureIndexList = new List<ArmatureIndexModel>();
            foreach (var item in armatures)
            {
                ArmatureIndexModel armatureIndex = new ArmatureIndexModel();
                armatureIndex.Id = item.Id;
                armatureIndex.Comment = item.Comment;
                armatureIndex.Amount = item.Amount;
                armatureIndex.Length = length.FirstOrDefault(x => x.Id == item.LengthId).Name;
                armatureIndex.Profile = profile.FirstOrDefault(x => x.Id == item.ProfileId).Name;
                armatureIndex.Khamut = item.Khamut;
                armatureIndex.Theoretical = item.Theoretical;
                armatureIndex.Glinula = item.Glinula;
                armatureIndexList.Add(armatureIndex);

            }
            WholeOrderModel model = new WholeOrderModel()
            {
                Order = order,
                Company = company,
                DriverList = order.DriverList,
                OrderDetails = orderDetails,
                Armatures = armatureIndexList,
                ArmatureProfile = profile,
                Lenght = length,
                CompanyAddress = company.CompanyAddressList.FirstOrDefault(x => x.Id == order.CompanyAddress)?.Address
            };

            return View(model);
        }




        // GET: Driver/Delete/5
        public ActionResult Delete(int id)
        {
            return DeleteOrder(id);
        }

        // POST: Driver/Delete/5
        [HttpPost]
        public ActionResult DeleteOrder(int id)
        {
            try
            {
                _orderService.DeleteOrder(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }














        [HttpPost]
        public string CalculateValuta(object date)
        {
            string url = "http://www.nbg.ge/rss.php?date=";
            url += date;
            XmlDocument rssXmlDoc = new XmlDocument();
            rssXmlDoc.Load(url);

            XmlNodeList rssNodes = rssXmlDoc.SelectNodes("//item/description");

            string innerXML = rssNodes.Item(0).InnerXml;

            int index = innerXML.IndexOf("USD");
            string value = "";
            if (index > 0)
            {
                value = innerXML.Substring(index + 1);
                index = value.IndexOf("<td>");
                value = value.Substring(index + 1);
                index = value.IndexOf("<td>");
                value = value.Substring(index + 4);
                value = value.Substring(0, 6);
            }
            return value;
        }


    }
}