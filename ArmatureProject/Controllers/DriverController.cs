﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess.Services;
using DataAccess.Entities;
using Newtonsoft.Json;

namespace ArmatureProject.Controllers
{
    [Authorize]
    public class DriverController : Controller
    {
        private readonly IDriverService _driverService;
        public DriverController(IDriverService driverService)
        {
            _driverService = driverService;
        }

        // GET: Company
        public ActionResult Index()
        {
            var model = _driverService.GetAllDrivers();

            return View(model);
        }


        [HttpPost]
        public string GetDriver(int driverId) {
            return JsonConvert.SerializeObject(_driverService.GetDriver(driverId));
        }

        // GET: Driver/Details/5
        public ActionResult Details(int id)
        {

            var model = _driverService.GetDriver(id);
            return View(model);
        }

        // GET: Driver/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Driver/Create
        [HttpPost]
        public ActionResult Create(Driver model)
        {
            try
            {
                var result = _driverService.AddDriver(model);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        // GET: Driver/Edit/5
        public ActionResult Edit(int id)
        {
            _driverService.GetDriver(id);
            return View();
        }




        [HttpPost]
        public string GetDriverList()
        {
            var model = _driverService.GetAllDrivers();
            return JsonConvert.SerializeObject(model);
        }






        // POST: Driver/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Driver model)
        {
            try
            {

                _driverService.AddDriver(model);

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        // GET: Driver/Delete/5
        public ActionResult Delete(int id)
        {
            return DeleteComp(id);
        }

        // POST: Driver/Delete/5
        [HttpPost]
        public ActionResult DeleteComp(int id)
        {
            try
            {
                _driverService.DeleteDriver(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
    }
}
