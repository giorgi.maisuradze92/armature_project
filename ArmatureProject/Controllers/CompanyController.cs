﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess.Entities;
using DataAccess.Repositories;
using DataAccess.Services;
using DataAccess.Infrastructure;
using Newtonsoft.Json;

namespace ArmatureProject.Controllers
{
    [Authorize]
    public class CompanyController : Controller
    {
        private ICompanyService _companyService;

     
        public CompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        // GET: Company
        public ActionResult Index()
        {
             var model = _companyService.GetAllCompanies();
            
            return View(model);
        }


        [HttpPost]
        public string GetCompany(int companyId)
        {
            var model = _companyService.GetCompany(companyId);
            model.CompanyAddressList = _companyService.GetCompanyAddresses(companyId);
            return JsonConvert.SerializeObject(model);
        }



        // GET: Driver/Details/5
        public ActionResult Details(int id)
        {

            var model = _companyService.GetCompany(id);
            Debug.Assert(model != null, "model != null");
            model.CompanyAddressList =  _companyService.GetCompanyAddresses(id);

            return View(model);
        }

        // GET: Driver/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Driver/Create
        [HttpPost]
        public ActionResult Create(Company model)
        {
            try
            {
                var result = _companyService.AddCompany(model);

                foreach (var itemAddress in model.CompanyAddressList)
                {
                    _companyService.AddCompanyAddress(new CompanyAddress()
                    {
                        CompanyId = result,
                        Address = itemAddress.Address
                    } );
                }
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        // GET: Driver/Edit/5
        public ActionResult Edit(int id)
        {
           var model =  _companyService.GetCompany(id);
            model.CompanyAddressList = _companyService.GetCompanyAddresses(id);
            return View(model);
        }

        // POST: Driver/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Company model)
        {
            try
            {
                var companyAddressIdList =  _companyService.GetCompanyAddresses(id).Select(x=>x.Id).ToList();
                foreach (var item in model.CompanyAddressList)
                {
                    companyAddressIdList.Remove(item.Id);
                }

                _companyService.AddCompany(model);

                foreach (var item in companyAddressIdList)
                {
                    _companyService.DeleteCompanyAddress((int)item);
                }
               



                foreach (var itemAddress in model.CompanyAddressList)
                {
                    _companyService.AddCompanyAddress(new CompanyAddress()
                    {
                        CompanyId = id,
                        Address = itemAddress.Address,
                        Id = itemAddress.Id
                    });
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Driver/Delete/5
        public ActionResult Delete(int id)
        {
            return DeleteComp(id);
        }

        // POST: Driver/Delete/5
        [HttpPost]
        public ActionResult DeleteComp(int id)
        {
            try
            {
                _companyService.DeleteCompany(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
    }
}