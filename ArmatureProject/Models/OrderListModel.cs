﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ArmatureProject.Models
{
    public class OrderListModel
    {
        public OrderListModel()
        {
            DriverNameList = new List<string>();
        }

        public int? OrderId { get; set; }

        [DisplayName("კომპანია")]
        public string CompanyName { get; set; }

        [DisplayName("მძღოლი")]
        public List<string> DriverNameList { get; set; }

        [DisplayName("შექმნის თარიღი")]
        public DateTime CreateDate { get; set; }

        [DisplayName("გაგზავნის Id")]
        public string SentId { get; set; }

        [DisplayName("არმატურის რაოდენობა")]
        public decimal? ArmatureAmount { get; set; }

    }



    public class OrderListModelSearch {
        public OrderListModelSearch()
        {
            CompanyList = new List<Company>();
            DriverList = new List<Driver>();
        }

        public List<OrderListModel> OrderListModel { get; set; }
        public List<Company> CompanyList { get; set; }
        public List<Driver> DriverList { get; set; }

    }
}