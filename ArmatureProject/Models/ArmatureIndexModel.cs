﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArmatureProject.Models
{
    public class ArmatureIndexModel
    {
        public int? Id { get; set; }

        public string Comment { get; set; }
        public string Profile { get; set; }
        public string Length { get; set; }
        public decimal? Amount { get; set; }
        public bool? Khamut { get; set; }
        public bool? Theoretical { get; set; }
        public bool? Glinula { get; set; }

    }
}