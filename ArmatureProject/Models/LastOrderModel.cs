﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccess.Entities;

namespace ArmatureProject.Models
{
    public class LastOrderModel
    {
        public LastOrderModel()
        {
            Order = new Order();
            OrderDetails = new List<DataAccess.Entities.OrderDetail>();
            Armatures = new List<Armature>();
            ArmatureLenghtList = new List<ArmatureLength>();
            ArmatureProfileList = new List<DataAccess.Entities.ArmatureProfile>();
            CompanyList = new List<Company>();
            DriverList = new List<Driver>();
        }
        public Order Order { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
        public List<Armature> Armatures { get; set; }
        public List<ArmatureLength> ArmatureLenghtList { get; set; }
        public List<ArmatureProfile> ArmatureProfileList { get; set; }
        public List<Company> CompanyList { get; set; }
        public List<Driver> DriverList { get; set; }
    }
}