﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArmatureProject.Models
{
    public class OrderCreateModel
    {
        public OrderCreateModel()
        {
            Order = new Order();
            OrderDetails = new List<DataAccess.Entities.OrderDetail>();
            Armatures = new List<ArmatureIndexModel>();
            ArmatureLenghtList = new List<ArmatureLength>();
            ArmatureProfileList = new List<DataAccess.Entities.ArmatureProfile>();
            CompanyList = new List<Company>();
            DriverList = new  List<Driver>();
        }
        public Order Order { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
        public List<ArmatureIndexModel> Armatures { get; set; }
        public List<ArmatureLength> ArmatureLenghtList { get; set; }
        public List<ArmatureProfile> ArmatureProfileList { get; set; }

        public SelectedObj Company { get; set; }
        public SelectedObj Driver { get; set; }

        public SelectedObj Length { get; set; }
        public SelectedObj Profile { get; set; }

        public List<Company> CompanyList { get; set; }
        public List<Driver> DriverList { get; set; }
    }

    public class SelectedObj {
        public int Value { get; set; }
        public string Text { get; set; }
    }
}