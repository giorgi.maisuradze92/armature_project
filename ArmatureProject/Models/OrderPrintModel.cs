﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArmatureProject.Models
{
    public class OrderPrintModel
    {

        public OrderPrintModel()
        {
            Company = new Company();
            DriverList = new List<Driver>();
            Detail = new List<OrderDetail>();
            Armature = new List<PrintArmature>();
            Order = new Order();

            DateTime dat = Convert.ToDateTime("02/05/2017");
        }

        public Order Order { get; set; }
        public Company Company { get; set; }
        public List<Driver> DriverList { get; set; }
        public List<OrderDetail> Detail { get; set;}
        public List<PrintArmature> Armature { get; set; }

        public string Theoretical { get; set; }
        public string Khamut { get; set; }
    }


    public class PrintArmature {

        public int? Id { get; set; }
        public decimal? Amount { get; set; }
        public int ProfileId { get; set; } 
        public int? OrderId { get; set; }
        public int LengthId { get; set; }
        public string Comment { get; set; }
        public string Theoretical { get; set; }
        public string Khamut { get; set; }
        public string Glinula { get; set; }

    }
}