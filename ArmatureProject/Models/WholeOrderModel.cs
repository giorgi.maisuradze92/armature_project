﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccess.Entities;

namespace ArmatureProject.Models
{
    public class WholeOrderModel
    {
        public WholeOrderModel()
        {
            Order = new Order();
            OrderDetails = new List<DataAccess.Entities.OrderDetail>();
            Armatures = new List<ArmatureIndexModel>();
            Lenght = new List<ArmatureLength>();
            ArmatureProfile = new List<DataAccess.Entities.ArmatureProfile>();
            Company = new Company();
            DriverList = new List<Driver>();
        }

        public string CompanyAddress { get; set; }
        public Order Order { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
        public List<ArmatureIndexModel> Armatures { get; set; }
        public List<ArmatureLength> Lenght { get; set; }
        public List<ArmatureProfile> ArmatureProfile { get; set; }
        public Company Company { get; set; }
        public List<Driver> DriverList { get; set; }
        public DateTime? EditDate { get; set; }
    }
}