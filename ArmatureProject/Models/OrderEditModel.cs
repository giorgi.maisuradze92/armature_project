﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArmatureProject.Models
{
    public class OrderEditModel
    {
        public OrderEditModel()
        {
            ArmatureList = new List<Armature>();
            OrderDetailList = new List<OrderDetail>();
            CompanyList = new List<Company>();
            DriverList = new List<Driver>();
            ArmatureLengthList = new List<ArmatureLength>();
            ArmatureLengthList = new List<ArmatureLength>();
            DriverIdList = new List<int?>();
            CompanyAddressList = new List<DataAccess.Entities.CompanyAddress>();
        }
        public int OrderId { get; set; }
        public int CompanyId { get; set; }
        public List<int?> DriverIdList { get; set; }
        public bool? IsPaid { get; set; }
        public bool? IsBuyer { get; set; }
        public string Comment { get; set; }
        public int? SendId { get; set; }
        public bool? Theoretical { get; set; }
        public bool? Khamut { get; set; }
        public string OrderHistory { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime? EditDate { get; set; }
        public int CompanyAddress { get; set; }
        public List<CompanyAddress> CompanyAddressList { get; set; }

        public List<Company> CompanyList { get; set; }
        public List<Driver> DriverList { get; set; }
        public List<ArmatureLength> ArmatureLengthList { get; set; }
        public List<ArmatureProfile> ArmatureProfileList { get; set; }

        public List<Armature> ArmatureList { get; set; }
        public List<OrderDetail> OrderDetailList { get; set; }

    }
}