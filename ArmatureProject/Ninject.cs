﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using DataAccess.Infrastructure;
using DataAccess.Repositories;
using DataAccess.Services;
using DataAccess.UnitOfWork;
using Microsoft.Practices.Unity;
using Ninject;

namespace ArmatureProject
{
    public class Ninject : DefaultControllerFactory
    {
        private readonly IKernel ninjectKernel;

        public Ninject()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings()
        {
            ninjectKernel.Bind<ICompanyService>().To<CompanyService>();
            ninjectKernel.Bind<IDriverService>().To<DriverService>();
            ninjectKernel.Bind<IOrderService>().To<OrderService>();

            ninjectKernel.Bind<IUnitOfWork>().To<UnitOfWork>();

            ninjectKernel.Bind<ICompanyRepository>().To<CompanyRepository>();
            ninjectKernel.Bind<IDriverRepository>().To<DriverRepository>();
            ninjectKernel.Bind<IOrderRepository>().To<OrderRepository>();
            ninjectKernel.Bind<IArmatureRepository>().To<ArmatureRepository>();
            ninjectKernel.Bind<IArmatureLengthRepository>().To<ArmatureLengthRepository>();
            ninjectKernel.Bind<IArmatureProfileRepository>().To<ArmatureProfileRepository>();
            ninjectKernel.Bind<IOrderDetailRepository>().To<OrderDetailRepository>();
            ninjectKernel.Bind<ICompanyAddressRepository>().To<CompanyAddressRepository>();

            ninjectKernel.Bind<IConnectionFactory>().To<ConnectionFactory>();
        }
    }
}